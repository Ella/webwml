#use wml::debian::translation-check translation="9ce946ad1768ef4407e37f730593301c420859e8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Divers problèmes de sécurité ont été découverts dans le planificateur CRON.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9525">CVE-2017-9525</a>

<p>Correction du crontab de groupe contre une élévation de privilèges à l’aide du script
postinst du paquet Debian, comme décrit par Alexander Peslyak (Solar Designer) dans
<a href="http://www.openwall.com/lists/oss-security/2017/06/08/3">http://www.openwall.com/lists/oss-security/2017/06/08/3</a></p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9704">CVE-2019-9704</a>

<p>Déni de service : correction de renvoi non vérifié de calloc(). Florian Weimer
a découvert qu’une vérification manquante pour la valeur de retour de calloc()
pourrait planter le démon. Cela pourrait être déclenché par une très grande
crontab créée par un utilisateur.</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9705">CVE-2019-9705</a>

<p>Limitation du nombre de lignes de crontab à mille pour empêcher un
utilisateur malveillant de créer une crontab excessive. Le démon écrira dans le
journal un avertissement pour les fichiers existants, et crontab(1) refusera
d’en créer de nouveaux.</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9706">CVE-2019-9706</a>

<p>Un utilisateur a signalé une condition d’utilisation de mémoire après
libération dans le démon cron, conduisant à un scénario possible de déni de
service par plantage du démon.</p>

</li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.0pl1-127+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets cron.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1723.data"
# $Id: $
