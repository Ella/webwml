#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été corrigées dans la bibliothèque GNU C de
Debian, eglibc :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1234">CVE-2016-1234</a>

<p>Alexander Cherepanov a découvert l'implémentation de glob de glibc était
victime d'un dépassement de pile quand elle était appelée avec
l'indicateur GLOB_ALTDIRFUNC et rencontrait un long nom de fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3075">CVE-2016-3075</a>

<p>L'implémentation de getnetbyname dans nss_dns était vulnérable à un
dépassement de pile et à un plantage si elle est invoquée pour un nom très
long.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3706">CVE-2016-3706</a>

<p>Michael Petlan a signalé que getaddrinfo copiait de grandes quantités de
données d'adresses, menant éventuellement à un dépassement de pile. Cela
complète le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2013-4458">CVE-2013-4458</a>.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.13-38+deb7u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets eglibc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-494.data"
# $Id: $
