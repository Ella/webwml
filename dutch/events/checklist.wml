#use wml::debian::template title="Checklist voor het opzetten van een stand"
# $Id$
#use wml::debian::translation-check translation="96da98ca0d7c98bdddb5d60c39c8e06987402075"

# Last Translation update by $Author$
# Last Translation update at $Date$

<p>Deze lijst tracht mensen te helpen die een stand voor het Debian-project
willen organiseren op een beurs. Stuur commentaar naar
<a href="mailto:events@debian.org">events@debian.org</a>.
</p>

<h3>Een stand organiseren</h3>

<p>Dit is een chronologische lijst. Sommige zaken kunnen evenwel parallel
worden uitgevoerd en natuurlijk kunnen sommige andere in een andere volgorde
worden gedaan.
</p>

<ol>

<li> Indien u op de hoogte bent van een gebeurtenis waaraan Debian
	 eventueel zou kunnen deelnemen, mail dan naar
	 <a href="eventsmailinglists">debian-events-&lt;regio&gt;</a>.
	 Dit geldt ook als u niet zelf zinnens bent een stand te bemannen.
</li>

<li> Een stand organiseren heeft enkel zin als het werk over twee of meer
     mensen verdeeld kan worden. Overweeg om hulp te vragen op
     <a href="eventsmailinglists">debian-events-&lt;regio&gt;</a>
     als u te weinig hulp heeft. Indien niemand anders kan of wil
     helpen, herziet u best het plan om een stand te organiseren.
</li>

<li> Als er enkele mensen bereid zijn om te helpen met de stand,
     vergewis u er dan van dat u over minstens één machine kunt
     beschikken om een demonstratie van Debian te geven. Een
     mogelijkheid is om gebruik te maken van de evenementenkoffer
     van Debian die voor dergelijke zaken bedoeld is. Vraag hem
     aan op events@debian.org. Indien u over geen enkele machine
     kunt beschikken, herziet u best het plan om een stand te organiseren,
     want zonder machines zou het nogal een lege bedoening zijn.
</li>

<li> Nu er èn mensen èn machines beschikbaar zijn, is het tijd om de
     administratie te vragen voor een gratis stand voor het project.
</li>

<li> Talrijke ervaringen met evenementen hebben geleerd dat het
     nuttig kan zijn om het evenement te coördineren op
     Debian Wiki aan de hand van het formulier op <a
     href="https://wiki.debian.org/DebianEvents#Adding_a_new_event">de
     Debian evenementenpagina</a> aldaar.
</li>

<li> Zorg ervoor dat de stand werkelijk ingericht zal zijn.
</li>

<li> Zorg ervoor dat de stand stroom en internet zal hebben.
</li>

<li> Ga na of er muren zullen zijn om posters tegen te hangen.
</li>

<li> Als er posters kunnen gebruikt worden, ga dan na of u grote posters
     kunt afdrukken. Of misschien kunnen er enkele naar u worden verstuurd
     (bekijk <a href="material">materiaal voor stands</a>). Debian kan
     ook de kosten vergoeden voor het drukken van posters op groot formaat.
</li>

<li> Ga na of u flyers kunt ontwerpen en printen zodat bezoekers iets
     mee kunnen nemen. (Bekijk <a href="material">materiaal voor stands</a>).
</li>

# <--<li> Als het een grote beurs is, controleer dan of er niemand cd's wil
#     produceren voor bezoekers. Controleer of u geen sponsors kunt vinden om
#     te betalen voor de productie zodat bezoekers ze kunnen meenemen zonder
#     betaling. Controleer dat het weggeven van enkel-binaire cd's in
#     <a href="$(HOME)/CD/vendors/legal">overeenstemming</a> is met de
#     licentie van de verspreide software.
#</li>--Hiertegen is men opgekomen, althans in Duitsland. Dit zou
# herschreven moeten worden. -->

<li> Als iemand van de bemanning van de stand een voordracht kan geven over
     Debian in het algemeen of over een specifieke invalshoek van het project,
     vraag die dan om de administratie te contacteren met de vraag of ze deze
     voordracht willen aanvaarden. In elk geval kunt u deze voordracht houden
     in de stand. Zorg dat u de planning op voorhand (de eerste dag)
     bekend maakt, zodat mensen op de hoogte zijn.
</li>

<li> Ook als er geen formele voordracht wordt gepland, kunt u de administratie
     een lokaal vragen om een 'Birds of a Feater (BOF)' te organiseren.
     Een BOF is een informele samenkomst tussen ontwikkelaars en gebruikers,
     zonder planning, waar iedereen om het even welke vraag kan stellen. Ze
     zijn zeer verhelderend omdat ontwikkelaars de kans krijgen om te weten
     te komen wat mensen denken en deze mensen een antwoord krijgen op de
     vragen die ze hebben.
</li>

<li> Onderhandel met de administratie over het aantal tafels en stoelen die
     vereist zijn voor de stand.
</li>

<li> Zorg ervoor dat er in de stand genoeg stekkerdozen voorhanden zullen
     zijn, zodat de machines werkelijk kunnen aangezet worden.
</li>

<li> Zorg voor genoeg netwerkkabels en hubs/switches in de stand,
     zodat de computers aangesloten kunnen worden.
</li>

<li> Kale tafels zien er niet uit. U kunt misschien witte (papieren)
     tafelkleden gebruiken zodat de stand er professioneler uit ziet.
</li>

<li> Als u iets wilt verkopen (t-shirts, cd's, pins, petjes, enz.), moet u
     eerst de administratie contacteren om te zien of dit wel is toegestaan.
     Er zijn enkele grote evenementen waar absoluut niets mag verkocht worden.
</li>

<li> Als de administratie akkoord gaat dat u verkoopt aan de stand, moet u
     nagaan of er geen lokale regel is die dit verbiedt (vooral als het
     evenement plaats vindt op zondag). Misschien kan de administratie van het
     evenement helpen.
</li>

<li> Als u verkoopt, mag de klemtoon niet liggen op geld verdienen aan de
     stand. Het kan ook vreemd overkomen dat u een gratis stand, enz.
     aanvraagt en dan iets gaat verkopen en daardoor <q>veel</q> geld
     verdient met de stand.
</li>

<li> Op sommige evenementen wordt een bijeenkomst gepland voor sprekers en
     standhouders. Controleer of er één zal zijn en hoe u kunt deelnemen als u
     geïnteresseerd bent. Als een vuistregel is zo'n bijeenkomst meestal
     leuk omdat u er mensen van andere projecten en bedrijven ontmoet en
     kunt ontspannen van de stressvolle beurs.
</li>

<li> Zorg ervoor om genoeg vingerafdrukken van uw GnuPG-sleutel mee te nemen
     zodat anderen ze kunnen <a href="keysigning">tekenen</a>. Vergeet ook
     geen identiteitskaart of rijbewijs om uw identiteit te kunnen aantonen.
</li>

<li> Als u geld kunt bemachtigen om meer dingen te doen op de stand, lees dan
     enkele goede ideeën over <a href="material">>merchandising op stands</a>.
</li>

<li> Wanneer mensen en machines beschikbaar zijn, verzeker u er dan van dat
     alle mensen in de stand de machines kunnen bedienen. Dit vereist een
     demo-account met een gekend wachtwoord voor alle mensen in de stand. Dit
     account moet toestaan om <code>sudo apt-get</code> uit te voeren als de
     machines verbonden zijn met het netwerk en toegang hebben tot een
     Debian-spiegelserver.
</li>

<li> Het zou leuk zijn als iemand van de stand een kort rapport zou
     schrijven na het evenement, zodat
     <a href="$(HOME)/News/weekly/">Debian Wekelijks Nieuws</a> er
     achteraf over zou kunnen berichten.
</li>

</ol>

<p>E-mails over organisatie van de stand en voordrachten zouden gestuurd moeten
worden naar één van de <a href="booth#ml">mailinglijsten</a> zodat ze publiek
zijn, gearchiveerd worden en meer geïnteresseerde mensen kunnen aantrekken.
</p>

<p>Als u dergelijke e-mails niet naar een publieke lijst wilt sturen, dan kunt
u ze altijd naar events@debian.org sturen zodat we enkele tips kunnen geven
indien vereist of nuttig. We zullen echter niet zelf in contact treden met de
administratie om uw inspanningen niet te niet te doen.</p>

<h3>De stand opzetten</h3>

<ul>

<li> Als u een professionele stand wilt opzetten, is het altijd goed om weten
     waar u jassen, overbodige schoenen, drank en andere dingen kunt plaatsen.
     Contacteer de administratie om te weten of er een kleine ruimte voor zal
     zijn.
</li>

<li> Ga na bij de administratie hoe u bij de stand kunt voordat de stand
     geopend is voor het publiek. Sommige beurzen vereisen speciale
     toegangskaarten.
</li>

<li> Op de beurs zou de ratio van ontwikkelaars per bezoeker niet groter mogen
     zijn dan twee (2.0). Het is leuk om een stand te hebben vol met mensen,
     maar het is niet erg uitnodigend als de stand al vol is met
     Debian-mensen en er geen bezoeker meer bij kan.
</li>

<li> Vergeet niet om een groepsfoto te maken van alle Debian-mensen die
     meehelpen. Dit is al regelmatig vergeten, maar het helpt om gezichten
     met namen te associëren terwijl het Debian ook minder anoniem maakt.
</li>

<li> Laat op het einde van de dag of van de beurs geen stand vol afval,
     zoals lege flessen, achter.
</li>

<li> Het ziet er niet goed uit als mensen in de stand enkel aan tafel achter
     hun computer zitten in plaats van aandacht te hebben voor de bezoekers.
</li>

</ul>

