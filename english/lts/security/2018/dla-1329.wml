<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>memcached version prior to 1.4.37 contains an Integer Overflow
vulnerability that can result in data corruption and deadlocks. This
attack is exploitable via network connectivity to the memcached
service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.13-0.2+deb7u4.</p>

<p>We recommend that you upgrade your memcached packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1329.data"
# $Id: $
