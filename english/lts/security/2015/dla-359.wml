<define-tag description>MySQL 5.5 packages added; end of support for MySQL 5.1</define-tag>
<define-tag moreinfo>
<p>Oracle, the upstream maintainer of MySQL, no longer supports MySQL
version 5.1, which is included in Debian 6.0 <q>squeeze</q>. MySQL 5.1 likely
suffers from multiple vulnerabilities fixed in newer versions after the
end of upstream support, but Oracle does not disclose enough information
either to verify or to fix them.</p>

<p>As an alternative, the Debian LTS team is providing MySQL 5.5 packages
for use in Debian 6.0 <q>squeeze</q>. We recommend that Squeeze LTS users
install them and migrate their MySQL databases.</p>

<p>Please note that a dist-upgrade will not consider these MySQL 5.5
packages automatically, so users need to install them explicitly.</p>

<p>If you are running a MySQL server:</p>

<pre>apt-get install mysql-server-5.5</pre>

<p>If you only need the MySQL client:</p>

<pre>apt-get install mysql-client-5.5</pre>


<h3>Compatibility updates</h3>

<p>Some packages were updated to solve incompatibility issues, that were
fixed in the following versions:</p>

<ul>
<li>bacula-director-mysql 5.0.2-2.2+squeeze2</li>
<li>cacti 0.8.7g-1+squeeze9</li>
<li>phpmyadmin 4:3.3.7-10</li>
<li>postfix-policyd 1.82-2+deb6u1</li>
<li>prelude-manager 1.0.0-1+deb6u1</li>
</ul>

<p>We recommend that you upgrade these packages before upgrading to
MySQL 5.5. A common dist-upgrade should be enough.</p>

<p>We have done our best to provide you with reliable MySQL 5.5 packages. We
have made available test packages for some time, although we did not get
any feedback from users. In any case, don't hesitate to report any issues
related to this MySQL upgrade to debian-lts@lists.debian.org.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2015/dla-359.data"
# $Id: $
