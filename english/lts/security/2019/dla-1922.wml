<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>hostapd (and wpa_supplicant when controlling AP mode) did not perform
sufficient source address validation for some received Management frames
and this could result in ending up sending a frame that caused
associated stations to incorrectly believe they were disconnected from
the network even if management frame protection (also known as PMF) was
negotiated for the association. This could be considered to be a denial
of service vulnerability since PMF is supposed to protect from this
type of issues.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.3-1+deb8u9.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1922.data"
# $Id: $
