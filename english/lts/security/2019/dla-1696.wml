<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Ceph, a distributed storage
and file system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14662">CVE-2018-14662</a>

    <p>It was found that authenticated ceph users with read only
    permissions could steal dm-crypt encryption keys used in ceph disk
    encryption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16846">CVE-2018-16846</a>

    <p>It was found that authenticated ceph RGW users can cause a denial of
    service against OMAPs holding bucket indices.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.80.7-2+deb8u3.</p>

<p>We recommend that you upgrade your ceph packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1696.data"
# $Id: $
