<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Felix Wilhelm of Google Project Zero discovered that HAProxy, a TCP/HTTP
reverse proxy, did not properly handle HTTP/2 headers. This would allow
an attacker to write arbitrary bytes around a certain location on the
heap, resulting in denial-of-service or potential arbitrary code
execution.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.8.19-1+deb10u2.</p>

<p>We recommend that you upgrade your haproxy packages.</p>

<p>For the detailed security status of haproxy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/haproxy">\
https://security-tracker.debian.org/tracker/haproxy</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4649.data"
# $Id: $
