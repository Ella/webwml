<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6450">CVE-2020-6450</a>

    <p>Man Yue Mo discovered a use-after-free issue in the WebAudio
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6451">CVE-2020-6451</a>

    <p>Man Yue Mo discovered a use-after-free issue in the WebAudio
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6452">CVE-2020-6452</a>

    <p>asnine discovered a buffer overflow issue.</p></li>

</ul>

<p>For the oldstable distribution (stretch), security support for chromium
has been discontinued.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 80.0.3987.162-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4654.data"
# $Id: $
